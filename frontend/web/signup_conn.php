<?php 
	
	include 'db_conn.php';

	if (isset($_POST['submit'])) {
		
		$first_name = mysqli_real_escape_string($db, $_POST['first_name']);
		$last_name = mysqli_real_escape_string($db, $_POST['last_name']);
		$email = mysqli_real_escape_string($db, $_POST['email']);
		$reg_no = mysqli_real_escape_string($db, $_POST['reg_no']);
		$phonenumber = mysqli_real_escape_string($db, $_POST['phonenumber']);
		$username = mysqli_real_escape_string($db, $_POST['username']);
		$pass1 = mysqli_real_escape_string($db, $_POST['pass1']);
		$pass2 = mysqli_real_escape_string($db, $_POST['pass2']);

		if (empty($first_name) || empty($last_name) || empty($email) || empty($reg_no) || empty($phonenumber) || empty($username) ||  empty($pass1) || empty($pass2)) {
			echo "<script>alert('Fill all the fields')</script>";
			echo "<script>window.open('../signup.php', '_self')</script>";
			exit();
			
		}else{
			//check if national id is taken
			$sql = "SELECT reg_no FROM students WHERE reg_no='reg_no'";
			$result = mysqli_query($db, $sql);
			$checkResult = mysqli_num_rows($result);
			if ($checkResult > 0) {
				header("Location: ../signup.php?Registration is already taken!!");
				exit();
			}else{
				//check if pass match
				if ($pass2 !== $pass1) {
					header("Location: ../signup.php?Passwords dont match!!");
					exit();
				}else{
					//check if username exists
					$u_name = "SELECT username FROM students WHERE username='$username'";
					$record = mysqli_query($db, $u_name);
					if (mysqli_num_rows($record) > 0) {
						header("Location: ../signup.php?Username has been taken!!");
						exit();
					}else{
						//hash the password
						$pwd = sha1($pass1);

						//insert details to dbs
						$query = "INSERT INTO students(first_name,last_name,email,reg_no,county,edu_level,username,password) VALUES('$first_name','$last_name','$email','$reg_no','$phonenumber','$username','$pwd')";
						mysqli_query($db,$query);
						echo "<script>alert('Registration is successfull')</script>";
							echo "<script>window.open('../login.php', '_self')</script>";
						exit();
					}
					
				}
			}
		}
	}

	 ?>