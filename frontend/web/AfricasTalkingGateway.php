<?php

/*

  # COPYRIGHT (C) 2014 AFRICASTALKING LTD <www.africastalking.com>                                                   
 
  AFRICAStALKING SMS GATEWAY CLASS IS A FREE SOFTWARE IE. CAN BE MODIFIED AND/OR REDISTRIBUTED                        
  UNDER THE TERMS OF GNU GENERAL PUBLIC LICENCES AS PUBLISHED BY THE                                                 
  FREE SOFTWARE FOUNDATION VERSION 3 OR ANY LATER VERSION                                                            
 
  THE CLASS IS DISTRIBUTED ON 'AS IS' BASIS WITHOUT ANY WARRANTY, INCLUDING BUT NOT LIMITED TO                       
  THE IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.                     
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,            
  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE       
  OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
*/

class AfricasTalkingGatewayException extends Exception  {}

class AfricasTalkingGateway
{
  protected $_username;
  protected $_apiKey;
  
  protected $_requestBody;
  protected $_requestUrl;
  
  protected $_responseBody;
  protected $_responseInfo;
     
  //Turn this on if you run into problems. It will print the raw HTTP response from our server
  const Debug             = false;
  
  const HTTP_CODE_OK      = 200;
  const HTTP_CODE_CREATED = 201;
  
  public function __construct($username_, $apiKey_, $environment_ = "production")
  {
    $this->_username     = $username_;
    $this->_apiKey       = $apiKey_;

    $this->_environment  = $environment_;
    
    $this->_requestBody  = null;
    $this->_requestUrl   = null;
    
    $this->_responseBody = null;
    $this->_responseInfo = null;    
  }
  
  
  //Messaging methods
  public function sendMessage($to_, $message_, $from_ = null, $bulkSMSMode_ = 1, Array $options_ = array())
  {
    if ( strlen($to_) == 0 || strlen($message_) == 0 ) {
      throw new AfricasTalkingGatewayException('Please supply both to and message parameters');
    }
    
    $params = array(
		    'username' => $this->_username,
		    'to'       => $to_,
		    'message'  => $message_,
		    );
    
    if ( $from_ !== null ) {
      $params['from']        = $from_;
      $params['bulkSMSMode'] = $bulkSMSMode_;
    }
    
    //This contains a list of parameters that can be passed in $options_ parameter
    if ( count($options_) > 0 ) {
      $allowedKeys = array (
			    'enqueue',
			    'keyword',
			    'linkId',
			    'retryDurationInHours'
			    );
			    
      //Check whether data has been passed in options_ parameter
      foreach ( $options_ as $key => $value ) {
	if ( in_array($key, $allowedKeys) && strlen($value) > 0 ) {
	  $params[$key] = $value;
	} else {
	  throw new AfricasTalkingGatewayException("Invalid key in options array: [$key]");
	}
      }
    }
    
    $this->_requestUrl  = $this->getSendSmsUrl();
    $this->_requestBody = http_build_query($params, '', '&');
    
    $this->executePOST();
    
    if ( $this->_responseInfo['http_code'] == self::HTTP_CODE_CREATED ) {
      $responseObject = json_decode($this->_responseBody);
      if(count($responseObject->SMSMessageData->Recipients) > 0)
	return $responseObject->SMSMessageData->Recipients;
	  
      throw new AfricasTalkingGatewayException($responseObject->SMSMessageData->Message);
    }
    
    throw new AfricasTalkingGatewayException($this->_responseBody);
  }                                       

  // Payments
  public function initiateMobilePaymentCheckout($productName_,
						$phoneNumber_,
						$currencyCode_,
						$amount_,
						$metadata_) {
    $this->_requestBody = json_encode(array("username"     => $this->_username,
					    "productName"  => $productName_,
					    "phoneNumber"  => $phoneNumber_,
					    "currencyCode" => $currencyCode_,
					    "amount"       => $amount_,
					    "metadata"     => $metadata_));
    $this->_requestUrl  = $this->getMobilePaymentCheckoutUrl();
    
    $this->executeJsonPOST();
    if($this->_responseInfo['http_code'] == self::HTTP_CODE_CREATED) {
      $response = json_decode($this->_responseBody);
      if ( $response->status == "PendingConfirmation") return $response->transactionId;
      else throw new AfricasTalkingGatewayException($response->description);
    }
    throw new AfricasTalkingGatewayException($this->_responseBody);
  }

  public function mobilePaymentB2CRequest($productName_,
					  $recipients_) {
    $this->_requestBody = json_encode(array("username"     => $this->_username,
					    "productName"  => $productName_,
					    "recipients"   => $recipients_));
    $this->_requestUrl  = $this->getMobilePaymentB2CUrl();
    
    $this->executeJsonPOST();
    if($this->_responseInfo['http_code'] == self::HTTP_CODE_CREATED) {
      $response = json_decode($this->_responseBody);
      $entries  = $response->entries;
      if (count($entries) > 0) return  $entries;      
      else throw new AfricasTalkingGatewayException($response->errorMessage);
    }
    throw new AfricasTalkingGatewayException($this->_responseBody);
  }

  public function mobilePaymentB2BRequest($productName_, $providerData_, $currencyCode_, $amount_, $metadata_) {
		if(!isset($providerData_['provider']) || strlen($providerData_['provider']) == 0)
			throw new AfricasTalkingGatewayException("Missing field provider");
		
		if(!isset($providerData_['destinationChannel']) || strlen($providerData_['destinationChannel']) == 0)
			throw new AfricasTalkingGatewayException("Missing field destinationChannel");

    if(!isset($providerData_['destinationAccount']) || strlen($providerData_['destinationAccount']) == 0)
      throw new AfricasTalkingGatewayException("Missing field destinationAccount");
		
		if(!isset($providerData_['transferType']) || strlen($providerData_['transferType']) == 0)
			throw new AfricasTalkingGatewayException("Missing field transferType");
		
		$params = array("username" => $this->_username,
										"productName"  => $productName_,
										"currencyCode" => $currencyCode_,
										"amount"=>$amount_,
										'provider' => $providerData_['provider'],
										'destinationChannel' => $providerData_['destinationChannel'],
                    'destinationAccount' => $providerData_['destinationAccount'],
										'transferType' => $providerData_['transferType'],
										'metadata' => $metadata_);
		
    $this->_requestBody = json_encode($params);
    $this->_requestUrl  = $this->getMobilePaymentB2BUrl();
    
    $this->executeJsonPOST();
    if($this->_responseInfo['http_code'] == self::HTTP_CODE_CREATED) {
      $response = json_decode($this->_responseBody);
      return $response;
    }
    throw new AfricasTalkingGatewayException($this->_responseBody);
  }