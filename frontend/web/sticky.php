<? php ?>
<head>
 <html> 
   <title>sticky notes</title>

   <!-- CSS -->
   <link rel="stylesheet" href="assets/css/main.css?version=1" />
   <link rel="stylesheet" href="assets/css/html5sticky.css?version=1" />

   <link href='http://fonts.googleapis.com/css?family=Architects+Daughter' rel='stylesheet' />

   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
   <script>!window.jQuery && document.write(unescape('%3Cscript src="assets/js/jquery1.6.2.js"%3E%3C/script%3E'))</script>

   <script src="assets/js/respond.min.js"></script>
   <script src="assets/js/modernizr.custom.23610.js"></script>
   <script src="assets/js/html5sticky.js"></script>
   <script src="assets/js/prettyDate.js"></script>
</head>
<body>

	<header id="head">

      <a href="index.html">
         <h1>Tuksticky <br /><small>sticky notes!</small></h1>
      </a>

      <div class="left topsection">
         <a href="#" id="addnote" class="tooltip blue-tooltip"><img src="assets/img/add.png" alt="Add a new sticky note"><span>Add a new sticky note</span></a>
         <a href="#" id="removenotes" class="tooltip blue-tooltip"><img src="assets/img/remove.png" alt="Remove all sticky notes"><span>Remove all sticky notes</span></a>
      </div>

      <div class="left topsection">
         <a href="#" id="shrink" class="tooltip blue-tooltip"><img src="assets/img/decrease.png" alt="Shrink"><span>Shrink sticky notes</span></a>
         <a href="#" id="expand" class="tooltip blue-tooltip"><img src="assets/img/increase.png" alt="Expand"><span>Expand sticky notes</span></a>
      </div>

	</header>

   <div id="main"></div>

   <div class="clear">&nbsp;</div>
   <div class="clear">&nbsp;</div>

</body>
</html>