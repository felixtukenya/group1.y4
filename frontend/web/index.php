<?php include('serveradmin.php') ?>
 <?php if (isset($_SESSION['message'])): ?>
  <div class="msg">
    <?php 
      echo $_SESSION['message']; 
      unset($_SESSION['message']);
    ?>
  </div>
<?php endif ?>
<!DOCTYPE html>
<html>
<head>
<title>Mass Notification System</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>      
<div>
  <h1 align="center">Mass Notification System</h1>
  <div class="container">
  <div class="header">
    <h2>Welcome admin!</h2>
  </div>
  <form method="post" action="serveradmin.php">
    <?php include('error.php'); ?>
    <div class="input-group">
      <input type="text" name="username" placeholder="Username" >
    </div><br>
    <div class="input-group">
      <input type="password" name="password" placeholder="Password">
    </div><br>
    <div>
      <button type="submit" class="btn" name="login_user">Login</button>
    </div><br>
  
</form>
</div>
</div>
</body>
</html>