<?php 
	
	include 'db_conn.php';

	if (isset($_POST['subscribe'])) {
		
		$first_name = mysqli_real_escape_string($db, $_POST['first_name']);
		$last_name = mysqli_real_escape_string($db, $_POST['last_name']);
		$reg_no = mysqli_real_escape_string($db, $_POST['reg_no']);
				
		$phonenumber = mysqli_real_escape_string($db, $_POST['phonenumber']);
		$email = mysqli_real_escape_string($db, $_POST['email']);
		

		if (empty($first_name) || empty($last_name) || empty($reg_no)  || empty($email) || empty($phonenumber)) {
			echo "<script>alert('Fill all the fields')</script>";
			echo "<script>window.open('../subscribe.php', '_self')</script>";
			exit();
			
		}
		else{
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
 			 echo "<script>alert('Email format is incorrect')</script>";
			echo "<script>window.open('../subscribe.php', '_self')</script>"; 
 			 exit();
}
	
		else{
			//check if registration no is taken
			$sql = "SELECT reg_no FROM students WHERE reg_no='reg_no'";
			$result = mysqli_query($db, $sql);
			$checkResult = mysqli_num_rows($result);
			if ($checkResult > 0) {
				header("Location: ../subscribe.php?Registration no is already taken!!");
				exit();
			}
			else{						

						//insert details to dbs
						$query = "INSERT INTO students(first_name,last_name,reg_no,phonenumber,email) VALUES('$first_name','$last_name','$reg_no','$phonenumber','$email')";
						mysqli_query($db,$query);
						echo "<script>alert('Subscription is successfull')</script>";
						echo "<script>window.open('../index.php', '_self')</script>";
						exit();
					}
					}	

		}
	}

	 ?>