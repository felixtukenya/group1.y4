<!DOCTYPE html>
<html>
<head>
<title>Mass Notification System</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="row">
	
		<div class="col-sm-2"></div>
		<div class="col-sm-8 header">
			<h1 align="center">MASS NOTIFICATION SYSTEM</h1>
			<nav class="navbar navbar-expand-lg  navbar-light pull-right" style="background-color: #bcc5ce">
             <ul class="nav nav-pills nav-fill">
               <li class="nav-item active">
                   <a class="nav-item nav-link" href="home.php">Home</a>
               </li>
               <li class="nav-item">
                   <a class="nav-item nav-link" href="send_email.php">Email</a>
               </li>
              <li class="nav-item">
                   <a class="nav-item nav-link" href="sms.php">Sms</a>
               </li>
               <li class="nav-item">
                   <a class="nav-item nav-link" href="sang/index.php">Scheduling</a>
               </li>
             </ul>
         </nav>
		</div>
		<div class="col-sm-2"></div>
		
	</div>
</body>